#include <SDL/SDL.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <cstdlib>
#include <vector>
#include <string>
#include <algorithm>
#include <fstream>
#include <cstdio>
#include <iostream>
#include <cmath>
#ifndef FUNCTION_H
#define FUNCTION_H


void drawSkybox(float size);
//void whiteWalkers();
//void drawWalker();
void initskybox();
void killskybox();
void lockCamera();
void moveCamera(float,float);
void moveCamerraUp(float,float);
void Control(float,float,bool);
void UpdateCamera();
unsigned int loadTexture(const char*);

#endif // FUNCTION_H
