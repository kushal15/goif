#include "function.h"

using namespace std;

bool mousein=false;
float angle=0.0;

void init()
{
    glClearColor(0.5,0.5,0.5,1.0);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(45,640.0/480.0,1.0,500.0);
    glMatrixMode(GL_MODELVIEW);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
    float col[]={1.0,1.0,1.0,1.0};
    glLightfv(GL_LIGHT0,GL_DIFFUSE,col);
    initskybox();
//    whiteWalkers();
}

void display()
{
    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
    glLoadIdentity();
    Control(0.1,0.1,mousein);    //calculate the position, and rotate the camera
    drawSkybox(150.0);            //draw the skybox here, so we apply the rotation, but we do not apply the translation to it.
//    drawWalker();
    //UpdateCamera();              //move the camera to the new location
    float pos[]={-1.0,1.0,-2.0,1.0};
    glLightfv(GL_LIGHT0,GL_POSITION,pos);

}

int main()
{
    SDL_Init(SDL_INIT_EVERYTHING);
    SDL_Surface* screen=SDL_SetVideoMode(640,480,32,SDL_SWSURFACE|SDL_OPENGL);
    bool running=true;
    Uint32 start;
    SDL_Event event;
    init();
    while(running)
    {
        start=SDL_GetTicks();
        while(SDL_PollEvent(&event))
        {
            switch(event.type)
            {
                case SDL_QUIT:
                        running=false;
                        break;
                case SDL_MOUSEBUTTONDOWN:
                        mousein=true;
                        SDL_ShowCursor(SDL_DISABLE);
                        break;
                case SDL_KEYDOWN:
                        if(event.key.keysym.sym==SDLK_p)
                        {
                                mousein=false;
                                SDL_ShowCursor(SDL_ENABLE);
                                break;
                        }
                        if(event.key.keysym.sym==SDLK_ESCAPE)
                        {
                                running=false;
                                break;
                        }
            }
        }
        display();
        SDL_GL_SwapBuffers();
        angle+=0.5;
        if(angle>360)
              angle-=360;
        if(1000/30>(SDL_GetTicks()-start))
              SDL_Delay(1000/30-(SDL_GetTicks()-start));
    }
    SDL_Quit();
    killskybox();
    return 0;
}
